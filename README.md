# CPTS-451-FINAL-PROJECT-JAD

This repository contains all of the work our team did for our final project for CPTS 451, Intro to Database Systems. 
In this file, we have all the WPF code for the app in JAD_app, the sql files for creating the database, the python file 
for parsing raw JSON data, and the ER model we designed.
