﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql; /* for db connectivity */

namespace JAD_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Business
        {
            public string business_id { get; set; }
            public string business_name { get; set;}
            public string business_address { get; set;}
            public string business_city { get; set;}
            public string business_state { get; set;}
            public int zipcode { get; set;}
            public double latitude { get; set;}
            public double longitude { get; set;}
            public int numtips { get; set;}
            public int numcheckins { get; set;}
            public bool isopen { get; set;}
            public double stars { get; set;}
        };
        public MainWindow()
        {
            InitializeComponent();
            addStates();
            addColumnsToBusinessGrid();
        }

        private string buildConnectionString()
        {
            return "Host = localhost; Username = postgres; Database = milestone2db; password = bitte"; // necessary evil
        }

        private void executeQuery(string sqlstr, Action<NpgsqlDataReader> myf)
        {
            using (var connection = new NpgsqlConnection(buildConnectionString()))
            {
                connection.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = sqlstr;
                    try
                    {
                        var reader = cmd.ExecuteReader(); // execute the cmd
                        while (reader.Read())             // read each row of returned list
                        {
                            myf(reader);                  // perform action on list row
                        }
                    }
                    catch (NpgsqlException ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                        System.Windows.MessageBox.Show($"SQL Error - {ex.Message.ToString()}");
                    }
                    finally // regardless of error or not
                    {
                        connection.Close();
                    }
                }
            }
        }

        private DataGridTextColumn addColumnToGrid(DataGrid grid, string bind_name, string header = "")
        {
            // each column is an object
            DataGridTextColumn col = new DataGridTextColumn();
            col.Binding = new Binding(bind_name);  // bind to name field of the business
            // set all properties
            col.Header = header;
            col.Width = DataGridLength.Auto;
            // add column to grid
            grid.Columns.Add(col);
            return col; // in case user wants to change the default settings
        }

        private void addColumnsToBusinessGrid()
        {
            addColumnToGrid(businessGrid, "business_name", "BusinessName");
            addColumnToGrid(businessGrid, "business_address", "Address");
            addColumnToGrid(businessGrid, "business_city", "City");
            addColumnToGrid(businessGrid, "business_state", "State");
            addColumnToGrid(businessGrid, "distance", "Distance (miles)");
            addColumnToGrid(businessGrid, "stars", "Stars");
            addColumnToGrid(businessGrid, "numtips", "# of Tips");
            addColumnToGrid(businessGrid, "numcheckins", "Total Checkins");
            //DataGridTextColumn col = addColumnToGrid(businessGrid, "business_id", "");
            //col.Width = 0;
        }

        private void addState(NpgsqlDataReader R)
        {
            stateList.Items.Add(R.GetString(0)); // add state to state combobox
        }

        private void addStates()
        {
            /* adds distinct states from database to the combo box */
            string sqlstr = "SELECT distinct business_state FROM business ORDER BY business_state";
            executeQuery(sqlstr, addState);
        }

        private void addCity(NpgsqlDataReader R)
        {
            cityList.Items.Add(R.GetString(0)); // add to city list
        }

        private void stateList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* when a state is selected, populate cities */
            cityList.Items.Clear();
            if (!(stateList.SelectedIndex > -1))
                return;
            /* adds distinct cities from database to the combobox */
            string sqlstr = $"SELECT distinct business_city FROM business WHERE business_state = '{stateList.SelectedItem.ToString()}' ORDER BY business_city";
            executeQuery(sqlstr, addCity);
        }

        private void addZipcode(NpgsqlDataReader R)
        {
            zipcodeList.Items.Add(R.GetInt32(0)); // add to zipcode list
        }

        private void cityList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* when a city is selected, populate zip codes */
            zipcodeList.Items.Clear();
            if (!(cityList.SelectedIndex > -1))
                return;
            /* adds distinct zipcodes from database to the combobox */
            string sqlstr = $"SELECT distinct zipcode FROM business WHERE business_state = '{stateList.SelectedItem.ToString()}' AND business_city = '{cityList.SelectedItem.ToString()}' ORDER BY zipcode";
            executeQuery(sqlstr, addZipcode);
        }

        private void addCategory(NpgsqlDataReader R)
        {
            categoryList.Items.Add(R.GetString(0)); // add to category list
        }

        private void zipcodeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /* when a zipcode is selected retrieve the business categories for the businesses that appear in that zipcode */
            categoryList.Items.Clear();
            if (!(zipcodeList.SelectedIndex > -1))
                return;
            /* adds distinct categories from database to the combobox */
            string sqlstr = $"SELECT distinct catagory_name FROM business, catagories WHERE business.business_id = catagories.business_id AND zipcode = '{zipcodeList.SelectedItem.ToString()}' ORDER BY catagory_name";
            executeQuery(sqlstr, addCategory);
        }

        private void addBusinessGridRow(NpgsqlDataReader R)
        {
        businessGrid.Items.Add(new Business() { 
                business_id = R.GetString(0), 
                business_name = R.GetString(1),
                business_address = R.GetString(2),
                business_city = R.GetString(3),
                business_state = R.GetString(4),
                zipcode = R.GetInt32(5),
                latitude = R.GetDouble(6),
                longitude = R.GetDouble(7),
                numtips = R.GetInt32(8),
                numcheckins = R.GetInt32(9),
                isopen = R.GetBoolean(10),
                stars = R.GetDouble(11)
            });
        }

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            /* when the user searches for businesses, all the businesses in the selected zipcode will be displayed */
            businessGrid.Items.Clear();
            if (!(zipcodeList.SelectedIndex > -1))
                return;

            string sqlstr = "";
            if (categoryFilterList.Items.IsEmpty)
            {
                sqlstr += $"SELECT business_id, business_name, business_address, business_city, business_state, zipcode, latitude, longitude, numtips, numcheckins, isopen, stars FROM business WHERE zipcode = '{zipcodeList.SelectedItem.ToString()}'";
            }
            else
            {
                string needed_cats = "(SELECT * FROM (VALUES"; // needed categories
                // all categories in filter category list must be associated with the given business_id in catagories table for the business with business_id to be included in the results
                foreach (var category in categoryFilterList.Items)
                {
                    // VALUES (category1), (category2), (category3), ...
                    needed_cats += $" ('{category.ToString()}'),";
                }
                needed_cats = needed_cats.TrimEnd(',');  // remove last comma
                needed_cats += ") AS needed_cats (catagory_name)) AS needed_cats";
                // query
                // temp is a table that holds the count of the rows of needed_cats
                // needed_cats is a table that holds all of the required categories
                // logic: filter catagories table by needed_cats using joins, then if a buisness's
                //        category count = needed count (in temp), they have all the requirements
                // note: there is probably a much cleaner and better way to do this
                sqlstr += $"SELECT catagories.business_id as business_id, business_name, business_address, business_city, business_state, zipcode, latitude, longitude, numtips, numcheckins, isopen, stars FROM (SELECT COUNT(*) AS needed_count FROM {needed_cats}) AS temp, ((catagories INNER JOIN {needed_cats} ON catagories.catagory_name = needed_cats.catagory_name) INNER JOIN business ON catagories.business_id = business.business_id) WHERE zipcode = '{zipcodeList.SelectedItem.ToString()}' GROUP BY catagories.business_id, business_name, business_address, business_city, business_state, zipcode, latitude, longitude, numtips, numcheckins, isopen, stars, needed_count HAVING COUNT(catagories.catagory_name) = needed_count;";
            }
            executeQuery(sqlstr, addBusinessGridRow);
        }

        private void businessGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /**/
            if (!(businessGrid.SelectedIndex > -1))
                return;
            Business business = businessGrid.SelectedItem as Business;
            if ((business.business_id != null) && (business.business_id.ToString().CompareTo("") != 0))
            {
                TipsWindow window = new TipsWindow(business.business_id.ToString());
                window.Show();
            }
        }
        
        private void addCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!(categoryList.SelectedIndex > -1))
                return;
            // don't allow duplicates
            foreach (var category in categoryFilterList.Items)
            {
                if (category.ToString().Equals(categoryList.SelectedItem.ToString()))
                    return;
            }
            categoryFilterList.Items.Add(categoryList.SelectedItem);
        }

        private void removeCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!(categoryFilterList.SelectedIndex > -1))
                return;
            categoryFilterList.Items.Remove(categoryFilterList.SelectedItem);
        }
    }
}
